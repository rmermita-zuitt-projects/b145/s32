const express = require('express');

const mongoose = require('mongoose');

const app = express();

const port = 5000;

//Connecting MongoDB
mongoose.connect("mongodb+srv://rem-ermita:pHa2retrac.@b145.s8xba.mongodb.net/session32activity?retryWrites=true&w=majority", 
{
	useNewUrlParser: true, useUnifiedTopology: true
}
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "There is an error with the connection"));

db.once("open", () => console.log("Successfully connected to the database"));


app.use(express.json());
app.use(express.urlencoded({extended: true}));


// Mongoose Schemas

const actSchema = new mongoose.Schema({

	email: String,
	username: String,
	password: String,
	age: Number,
	isAdmin: {
		type: Boolean,
		default: false
	}
});

//Model
const actTask = mongoose.model("actTask", actSchema);

app.post('/users/signup', (req, res) => {

	actTask.findOne({email : req.body.email}, (err, result) => {

		if(result != null && result.name === req.body.name){

			return res.send(`Email is already registered.`)

		} else {

			let newUser = new actTask({
				email : req.body.email,
				username: req.body.username,
				password: req.body.password,
				age: req.body.age
			});

			newUser.save((saveErr, savedUser) => {
				if(saveErr){
					return console.error(saveErr)
				} else {
					return res.status(200).send(`New user created : ${savedUser}`)
				};
			});
		};
	});
});

app.get("/users", (req, res) => {

	actTask.find({}, (err, result) => {

		if(err){
			
			return console.log(err);
		
		} else {

			return res.status(200).json({
				users : result
			})
		}
	})
});

app.put("/users/update-user/:wildcard", (req, res) => {

	let wildcard = req.params.wildcard
	let username = req.body.username

	actTask.findByIdAndUpdate(wildcard, {username: username}, (err, updatedUsername) => {
		if(err){
			console.log(err)
		} else {
			res.send(`User profile updated!`);
		}
	})
});

app.delete("/users/archive-user/:wildcard", (req, res) => {

	let wildcard = req.params.wildcard;

	actTask.findByIdAndDelete(wildcard, (err, deletedUser) => {
		if(err){
			console.log(err)
		} else {
			res.send(`${deletedUser.username} has been deactivated.`)
		}
	})
})

app.listen(port, () => console.log(`Running at port ${port}`));